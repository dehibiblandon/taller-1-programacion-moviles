package com.db.taller1.Model;
import java.io.Serializable;
public class Empleado implements  Serializable{

    private String nombreEmpleado;
    private String direccionEmpleado;
    private String telefonoEmpleado;
    private String cargoEmpleado;
    private String ciudadEmpleado;

    public Empleado(String nombreEmpleado, String direccionEmpleado, String telefonoEmpleado, String cargoEmpleado, String ciudadEmpleado) {
        this.nombreEmpleado = nombreEmpleado;
        this.direccionEmpleado = direccionEmpleado;
        this.telefonoEmpleado = telefonoEmpleado;
        this.cargoEmpleado = cargoEmpleado;
        this.ciudadEmpleado = ciudadEmpleado;
    }

    public Empleado (){

    }

    public String getNombreEmpleado() {
        return nombreEmpleado;
    }

    public void setNombreEmpleado(String nombreEmpleado) {
        this.nombreEmpleado = nombreEmpleado;
    }

    public String getDireccionEmpleado() {
        return direccionEmpleado;
    }

    public void setDireccionEmpleado(String direccionEmpleado) {
        this.direccionEmpleado = direccionEmpleado;
    }

    public String getTelefonoEmpleado() {
        return telefonoEmpleado;
    }

    public void setTelefonoEmpleado(String telefonoEmpleado) {
        this.telefonoEmpleado = telefonoEmpleado;
    }

    public String getCargoEmpleado() {
        return cargoEmpleado;
    }

    public void setCargoEmpleado(String cargoEmpleado) {
        this.cargoEmpleado = cargoEmpleado;
    }

    public String getCiudadEmpleado() {
        return ciudadEmpleado;
    }

    public void setCiudadEmpleado(String ciudadEmpleado) {
        this.ciudadEmpleado = ciudadEmpleado;
    }
}

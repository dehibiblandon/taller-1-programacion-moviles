package com.db.taller1.Model;

import java.io.Serializable;

public class Producto implements Serializable {
    private String nombre;
    private String cantidad;
    private String urlImage;

    public Producto (String nombre, String cantidad, String urlImage){
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.urlImage= urlImage;
    }

    public Producto() {

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }
}

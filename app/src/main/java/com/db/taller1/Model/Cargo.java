package com.db.taller1.Model;

import android.widget.EditText;

import java.io.Serializable;

public class Cargo implements Serializable {
    private String nombreCargo;

    public Cargo (String nombre){
        this.nombreCargo = nombre;
    }
    public  Cargo(){

    }

    public String getNombre() {
        return nombreCargo;
    }
    public void setNombre(String nombre) {
        this.nombreCargo = nombre;
    }

}

package com.db.taller1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.db.taller1.Adapter.ProductAdapter;
import com.db.taller1.Model.Empleado;
import com.db.taller1.Model.Producto;

import java.util.ArrayList;

import android.widget.Button;
import android.widget.Toast;

public class Modulo_Productos extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private Button agregarProducto;
    private Button salir;

    private ListView listProduct;
    private ArrayList<Producto> producto = new ArrayList<Producto>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modulo_productos);
        listProduct=findViewById(R.id.listaPrincipalProdutos);
        Bundle intent = getIntent().getBundleExtra("extra");
        producto= (ArrayList<Producto>) (intent.getSerializable("lista"));
        ProductAdapter adapter = new ProductAdapter(this, producto);// se le pasa la actividad y el array
        listProduct.setAdapter(adapter);
        listProduct.setOnItemClickListener(this);

        agregarProducto= findViewById(R.id.btnNuevoProducto);
        agregarProducto.setOnClickListener(this);
        salir = findViewById(R.id.btnSalirModProductos);
        salir.setOnClickListener(this);

    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(this, "elemento" +producto.get(i).getNombre(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.btnNuevoProducto:
                intent  = new Intent(this, Mod_Product_Crear_Producto.class);
                startActivity(intent);
                break;
            case R.id.btnSalirModProductos:
                intent  = new Intent(this, Modulos.class);
                startActivity(intent);
                break;

        }
    }
}
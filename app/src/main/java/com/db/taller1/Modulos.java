package com.db.taller1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Modulos extends AppCompatActivity implements View.OnClickListener {
    private Button btnModAdmin;
    private Button btnModProd;
    private Button btnModVentas;
    private Button btnAgregarProductos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modulos);
        btnModAdmin = findViewById(R.id.btnModAdminCrearCargos);
        btnModProd = findViewById(R.id.btnAdminCrearEmpleado);
        btnAgregarProductos = findViewById(R.id.createProduct);
        btnModVentas = findViewById(R.id.btnAdministrarEmpleados);

        btnModAdmin.setOnClickListener(this);
        btnModProd.setOnClickListener(this);
        btnAgregarProductos.setOnClickListener(this);
        btnModVentas.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.btnModAdminCrearCargos:
                intent  = new Intent(this, Modulo_Administrativo.class);
                // envio de datos
                startActivity(intent);

                break;
            case R.id.btnAdminCrearEmpleado:
                intent  = new Intent(this, Modulo_Productos.class);
                startActivity(intent);
                break;
            case R.id.btnAdministrarEmpleados:
                intent  = new Intent(this, Modulo_Ventas.class);
                startActivity(intent);
                break;
            case R.id.createProduct:
                intent  = new Intent(this, Mod_Product_Crear_Producto.class);
                startActivity(intent);
                break;
        }
    }
}
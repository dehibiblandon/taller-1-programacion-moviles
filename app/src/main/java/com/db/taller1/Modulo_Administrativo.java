package com.db.taller1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Modulo_Administrativo extends AppCompatActivity implements View.OnClickListener {

    private Button btnCrearCargo;
    private Button btnAdministrarCargo;
    private Button btnCrearEmpleado;
    private Button btnAdministrarEmpleado;
    private Button salir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modulo_administrativo);
        btnCrearCargo = findViewById(R.id.btnModAdminCrearCargos);
        btnAdministrarCargo = findViewById(R.id.btnAdministrarCargos);
        btnCrearEmpleado = findViewById(R.id.btnAdminCrearEmpleado);
        btnAdministrarEmpleado = findViewById(R.id.btnAdministrarEmpleados);
        salir = findViewById(R.id.btnSalirModAdmin);

        btnCrearCargo.setOnClickListener(this);
        btnAdministrarCargo.setOnClickListener(this);
        btnCrearEmpleado.setOnClickListener(this);
        btnAdministrarEmpleado.setOnClickListener(this);
        salir.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.btnModAdminCrearCargos:
                intent  = new Intent(this, Mod_Admin_Crear_Cargos.class);
                startActivity(intent);

                break;
            case R.id.btnAdministrarCargos:
                intent  = new Intent(this, Mod_Admin_Administrar_Cargos.class);
                startActivity(intent);
                break;
            case R.id.btnAdminCrearEmpleado:
                intent  = new Intent(this, Mod_Admin_Crear_Empleado.class);
                startActivity(intent);
                break;
            case R.id.btnAdministrarEmpleados:
                intent  = new Intent(this, Mod_Admin_Administrar_Empleados.class);
                startActivity(intent);
                break;
            case R.id.btnSalirModAdmin:
                intent  = new Intent(this, Modulos.class);
                startActivity(intent);
                break;
        }
    }
}
package com.db.taller1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Modulo_Ventas extends AppCompatActivity implements View.OnClickListener {
    private Button salir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modulo_ventas);
        salir = findViewById(R.id.btnSalirModVentas);

        salir.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.btnSalirModVentas:
                intent = new Intent(this, Modulos.class);
                startActivity(intent);
                break;
        }
    }
}
package com.db.taller1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.db.taller1.Model.Empleado;

import java.util.ArrayList;
import java.util.LinkedList;

public class Mod_Admin_Crear_Empleado extends AppCompatActivity implements View.OnClickListener {

    private Button btnGuardarEmpleado;
    private EditText nombreEmpleado;
    private EditText direccionEmpleado;
    private EditText telefonoEmpleado;
    private Spinner cargoEmpleado;
    private EditText ciudadEmpleado;
    private Button btnVerListado;
    private LinkedList<Empleado> empleados;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mod_admin_crear_empleado);
        empleados = new LinkedList<>();
        btnGuardarEmpleado = findViewById(R.id.btnGuardarEmpleado);

        nombreEmpleado = findViewById(R.id.nombreEmpleado);
        direccionEmpleado = findViewById(R.id.direccionEmpleado);
        telefonoEmpleado = findViewById(R.id.telefonoEmpleado);
    //cargoEmpleado = findViewById(R.id.listaCargos);
        ciudadEmpleado = findViewById(R.id.ciudadEmpleado);

        btnVerListado = findViewById(R.id.btnVerListadoEmpleados);
        btnGuardarEmpleado.setOnClickListener(this);
        btnVerListado.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnGuardarEmpleado:
                Empleado empleado = new Empleado();
                empleado.setNombreEmpleado(nombreEmpleado.getText().toString());
                empleado.setDireccionEmpleado(direccionEmpleado.getText().toString());
                empleado.setCargoEmpleado("Cargo quedamo");
                empleado.setCiudadEmpleado(ciudadEmpleado.getText().toString());
                empleado.setTelefonoEmpleado(telefonoEmpleado.getText().toString());
                empleados.add(empleado);
                //limpiar datos
                this.clearForm();
                Toast.makeText(this, "empleado " +empleado.getNombreEmpleado()+" registrado con éxito", Toast.LENGTH_SHORT).show();
                break;


            case R.id.btnVerListadoEmpleados:
                Bundle extra = new Bundle();
                extra.putSerializable("lista", empleados);
                Intent intent  = new Intent(Mod_Admin_Crear_Empleado.this, Mod_Admin_Administrar_Empleados.class);
                intent.putExtra("extra", extra);
                startActivity(intent);
                break;
        }
    }
    public void clearForm(){
        nombreEmpleado.getText().clear();
        direccionEmpleado.getText().clear();
        ciudadEmpleado.getText().clear();
        telefonoEmpleado.getText().clear();

    }
}
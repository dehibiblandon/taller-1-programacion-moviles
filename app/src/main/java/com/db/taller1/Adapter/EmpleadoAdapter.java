package com.db.taller1.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.db.taller1.Model.Empleado;
import com.db.taller1.R;

import java.util.ArrayList;

public class EmpleadoAdapter extends BaseAdapter {
    protected Activity activity;
    protected ArrayList<Empleado> empleado;

    public EmpleadoAdapter(Activity activity, ArrayList<Empleado> empleado) {
        this.activity = activity;
        this.empleado = empleado;
    }

    @Override
    public int getCount() {
        return empleado.size();
    }

    @Override
    public Object getItem(int i) {
        return empleado.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
       View v =  view;
       //validar si esta null
        if (view==null){
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.item_empleado, null);
        }
        Empleado empleadoElemento = empleado.get(i);

        //Enviar datos a la vista en cada campo
        TextView nombreEmpleado = v.findViewById(R.id.textNombreEmpleado);
        nombreEmpleado.setText(empleadoElemento.getNombreEmpleado());

        TextView direccionEmpleado = v.findViewById(R.id.textdireccionEmpleado);
        direccionEmpleado.setText(empleadoElemento.getDireccionEmpleado());

        TextView cargoEmpleado = v.findViewById(R.id.textCargoEmpleado);
        cargoEmpleado.setText(empleadoElemento.getCargoEmpleado());

        TextView ciudadEmpleado = v.findViewById(R.id.textTelefonoEmpleado);
        ciudadEmpleado.setText(empleadoElemento.getTelefonoEmpleado());

        TextView telefonoEmpleado = v.findViewById(R.id.textCiudadEmpleado);
        telefonoEmpleado.setText(empleadoElemento.getCiudadEmpleado());

                return  v;
    }
}

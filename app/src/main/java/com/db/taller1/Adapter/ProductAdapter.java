package com.db.taller1.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.db.taller1.Model.Producto;
import com.db.taller1.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProductAdapter extends BaseAdapter {

    protected Activity activity;
    protected ArrayList<Producto> producto;

    //Constructor

    public ProductAdapter (Activity activity, ArrayList<Producto> producto){
        this.activity = activity;
        this.producto = producto;
    }
    @Override
    public int getCount() {
        return producto.size();
    }

    @Override
    public Object getItem(int i) {
        return producto.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        if(view == null){
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.item_producto, null);
        }
        Producto productoElemento = producto.get(i);
        //Enviar datos a la vista en cada campo
        TextView nombreProduct = v.findViewById(R.id.textNombreProducto);
        nombreProduct.setText(productoElemento.getNombre());

        TextView inventario = v.findViewById(R.id.textCantidadProducto);
        inventario.setText(productoElemento.getCantidad());

        ImageView url = v.findViewById(R.id.urlImage);
        Picasso.get().load(productoElemento.getUrlImage()).into(url);


        return v;
    }

}

package com.db.taller1.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.db.taller1.Model.Cargo;
import com.db.taller1.Model.Empleado;
import com.db.taller1.R;

import java.util.ArrayList;

public class CargoAdapter extends BaseAdapter {
    protected Activity activity;
    protected ArrayList<Cargo> cargo;

    public CargoAdapter(Activity activity, ArrayList<Cargo> cargo) {
        this.activity = activity;
        this.cargo = cargo;
    }

    @Override
    public int getCount() {
        return cargo.size();
    }

    @Override
    public Object getItem(int i) {
        return cargo.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v =  view;
        //validar si esta null
        if (view==null){
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.item_cargo, null);
        }
        Cargo cargoElemento =  cargo.get(i);
        //Enviar datos a la vista en cada campo
        TextView nombreCargo = v.findViewById(R.id.textNombreCargo);
        nombreCargo.setText(cargoElemento.getNombre());


        return v;
    }
}

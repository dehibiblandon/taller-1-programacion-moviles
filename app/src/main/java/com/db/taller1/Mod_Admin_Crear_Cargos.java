package com.db.taller1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.db.taller1.Model.Cargo;
import com.db.taller1.Model.Empleado;

import java.util.LinkedList;

public class Mod_Admin_Crear_Cargos extends AppCompatActivity implements View.OnClickListener {

    private Button btnGuardarCargo;
    private Button btnVerListado;
    private EditText nombreCargo;
    private LinkedList<Cargo> cargos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mod_admin_crear_cargos);
        cargos = new LinkedList<>();
        btnGuardarCargo= findViewById(R.id.btnGuardarCargo);
        nombreCargo = findViewById(R.id.textInputNombreCargo);

        btnVerListado= findViewById(R.id.btnVolverAdminCargos);

        btnGuardarCargo.setOnClickListener(this);
        btnVerListado.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.btnGuardarCargo:
                Cargo cargo = new Cargo();
                cargo.setNombre(nombreCargo.getText().toString());
                cargos.add(cargo);
                //limpiar datos
                this.clearForm();
                Toast.makeText(this, "cargo " +cargo.getNombre()+" registrado con éxito", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnVolverAdminCargos:
                Bundle extra = new Bundle();
                extra.putSerializable("lista", cargos);
                 intent  = new Intent(Mod_Admin_Crear_Cargos.this, Mod_Admin_Administrar_Cargos.class);
                intent.putExtra("extra", extra);
                startActivity(intent);
                break;


        }
    }
    public void clearForm(){
        nombreCargo.getText().clear();
    }
}
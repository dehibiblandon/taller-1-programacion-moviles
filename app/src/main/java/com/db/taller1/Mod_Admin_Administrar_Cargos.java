package com.db.taller1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.db.taller1.Adapter.CargoAdapter;
import com.db.taller1.Adapter.EmpleadoAdapter;
import com.db.taller1.Model.Cargo;
import com.db.taller1.Model.Empleado;

import java.util.ArrayList;

public class Mod_Admin_Administrar_Cargos extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {


    private Button btnListCargoAtras;
    private ListView listViewCargo;
    private ArrayList<Cargo> listCargo = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mod_admin_administrar_cargos);
        listViewCargo = findViewById(R.id.listVCargo);
        Bundle intent = getIntent().getBundleExtra("extra");
        listCargo= (ArrayList<Cargo>) (intent.getSerializable("lista"));
        CargoAdapter adapter = new CargoAdapter(this, listCargo);
        listViewCargo.setAdapter(adapter);
        listViewCargo.setOnItemClickListener(this);
        btnListCargoAtras = findViewById(R.id.btnVolverModAdmin);

        btnListCargoAtras.setOnClickListener(this);

    }
    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.btnVolverModAdmin:
                intent  = new Intent(this, Modulo_Administrativo.class);
                startActivity(intent);
                break;

        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(this, "elemento " +listCargo.get(i).getNombre(), Toast.LENGTH_SHORT).show();
    }
}
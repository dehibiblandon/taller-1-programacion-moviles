package com.db.taller1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.db.taller1.Adapter.EmpleadoAdapter;
import com.db.taller1.Model.Empleado;

import java.util.ArrayList;

public class Mod_Admin_Administrar_Empleados extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private Button btnListEmpleadosAtras;
    private ListView listViewEmpleados;
    private ArrayList<Empleado> listEmpleado = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mod_admin_administrar_empleados);
        listViewEmpleados = findViewById(R.id.listVEmpleados);
        Bundle intent = getIntent().getBundleExtra("extra");
        listEmpleado= (ArrayList<Empleado>) (intent.getSerializable("lista"));
        EmpleadoAdapter adapter = new EmpleadoAdapter(this, listEmpleado);
        listViewEmpleados.setAdapter(adapter);
        listViewEmpleados.setOnItemClickListener(this);
        btnListEmpleadosAtras = findViewById(R.id.btnVolverModAdmin);

        btnListEmpleadosAtras.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.btnVolverModAdmin:
                intent  = new Intent(this, Modulo_Administrativo.class);
                startActivity(intent);
                break;

        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(this, "elemento" +listEmpleado.get(i).getNombreEmpleado(), Toast.LENGTH_SHORT).show();
    }
}
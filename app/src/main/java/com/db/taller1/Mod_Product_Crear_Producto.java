package com.db.taller1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.db.taller1.Model.Empleado;
import com.db.taller1.Model.Producto;

import java.util.LinkedList;

public class Mod_Product_Crear_Producto extends AppCompatActivity implements View.OnClickListener {
    private Button btnGuardarProducto;
    private Button volver;

    private EditText nombreProducto;
    private EditText cantidadProducto;
    private EditText urlImagen;
    private LinkedList<Producto> productos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mod_product_crear_producto);
        productos = new LinkedList<>();
        btnGuardarProducto= findViewById(R.id.btnGuardarProducto);
        volver = findViewById(R.id.btnVolverListadoProducto);

        nombreProducto = findViewById(R.id.textInputNombreProducto);
        cantidadProducto = findViewById(R.id.textCantidad);
        urlImagen = findViewById(R.id.textUrlProduct);

        volver.setOnClickListener(this);
        btnGuardarProducto.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        Intent intent;
        switch (view.getId()) {
            case R.id.btnGuardarProducto:
                Producto producto = new Producto();
                producto.setNombre(nombreProducto.getText().toString());
                producto.setCantidad(cantidadProducto.getText().toString());
                producto.setUrlImage(urlImagen.getText().toString());
                productos.add(producto);
                this.clearForm();
                Toast.makeText(this, "producto " +producto.getNombre()+" registrado con éxito", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnVolverListadoProducto:
                Bundle extra = new Bundle();
                extra.putSerializable("lista", productos);
                intent  = new Intent(Mod_Product_Crear_Producto.this, Modulo_Productos.class);
                intent.putExtra("extra", extra);
                startActivity(intent);
                break;
        }
    }

    private void clearForm() {
        nombreProducto.getText().clear();
        cantidadProducto.getText().clear();
        urlImagen.getText().clear();
    }
}